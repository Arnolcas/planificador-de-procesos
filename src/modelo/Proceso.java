/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arny
 */
public class Proceso {

    private String iD;
    private int inT;//tiempo de llegada
    private int cpuT;//tiempo en cpu
    private int cpuTres; //contador de tiempo restante
    private int pR;//prioridad
    private int sT = -1;//tiempo de inicio
   // private int sTaux;//tiempo de inicio  Aux
    private int eT = 0;//tiempo de final
    //private int eTaux;//tiempo de final Aux
    private int wT = 0;//tiempo de espera
    private int state = 0;//estado 0.esperadno 1.ejecutando 2.finalizado

    public Proceso(String iD, int cpuT) {

    }

    public Proceso(String iD, int iT, int cpuT, int pR) {
        this.iD = iD;
        this.inT = iT;
        this.cpuT = this.cpuTres = cpuT;
        this.pR = pR;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void ejecutando() {
        cpuTres--;
    }

    public void esperando() {
        
        wT++;
    }

    public int getsT() {
        return sT;
    }

    public void setsT(int sT) {
        this.sT = sT;
    }

    public int geteT() {
        return eT;
    }

    public void seteT(int eT) {
        this.eT = eT;
    }

    public int getcpuTres() {
        return cpuTres;
    }

    public int getinT() {
        return this.inT;
    }

    public String getId() {
        return this.iD;
    }
    
    public int getwT(){
        return this.wT;
    }

    public boolean equals(Proceso proceso) {
        if (this.iD.equals(proceso.getId())) {
            return true;
        }
        return false;
    }
    
    public int getCpuT(){
        return this.cpuT;
    }
}
