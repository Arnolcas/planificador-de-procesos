/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Planificador;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arny
 */
public class Sjf implements Runnable {

    ArrayList<Proceso> cola = new ArrayList<Proceso>();
    private Thread thread;
    private Planificador controlador;
    private int hz;

    public Sjf(ArrayList cola, Planificador controlador) {
        this.cola = (ArrayList) cola.clone();
        this.controlador = controlador;
        this.thread = new Thread(this, "SJF");
    }

    public void start() {
        if (!this.thread.isAlive()) {
            this.thread.start();
        }

    }

    @Override
    public void run() {
        this.controlador.working();
        this.hz = 0;
        Proceso activo = null;
        Proceso varaux = null;
        Proceso var = null;
        boolean buc = true;

        do {
            boolean ok = false;
            /**
             * ***************************************************************
             */
            Iterator<Proceso> itProceso = this.cola.iterator();
            while (itProceso.hasNext()) {
                varaux = itProceso.next();
                if (!(varaux.getState() == 2)) {
                    if (varaux.getinT() <= this.hz) {
                        if (varaux.getState() == 1) {
                            if (varaux.getcpuTres() == 0) {
                                if (!(varaux.getState() == 2)) {
                                    varaux.setState(2);
                                    varaux.seteT(this.hz);
                                }
                            } else {
                                activo = varaux;
                            }
                        }

                    }
                }
            }
            varaux = var = null;
            if (activo == null) {
                itProceso = this.cola.iterator();
                while (itProceso.hasNext()) {
                    varaux = itProceso.next();
                    if (varaux.getinT() <= this.hz) {
                        if (varaux.getState() == 0) {
                            if (activo == null) {
                                activo = varaux;
                            } else {
                                if (varaux.getCpuT() <= activo.getCpuT()) {
                                    activo = varaux;
                                }
                            }
                        }
                    }

                }
            }

            if (activo != null) {
                if (activo.getsT() == -1) {
                    activo.setsT(this.hz);
                }
                activo.ejecutando();
                if (activo.getState() == 0) {
                    activo.setState(1);
                }
            }
            itProceso = this.cola.iterator();
            while (itProceso.hasNext()) {
                varaux = itProceso.next();
                if (varaux != activo) {
                    if ((varaux.getinT() <= this.hz) && (varaux.getState() == 0)) {
                        varaux.esperando();
                    }
                }
            }

            activo = var = varaux = null;

            itProceso = this.cola.iterator();
            while (itProceso.hasNext()) {
                var = itProceso.next();
                if (var.getState() == 2) {
                    buc = false;
                } else {
                    buc = true;
                    this.hz++;
                    try {
                        sleep(500);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Sjf.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
            }
            activo = var = varaux = null;
        } while (buc);
        this.controlador.free();
        System.out.println("Final SJF");
    }

    public int getHz() {
        return this.hz;
    }
}
