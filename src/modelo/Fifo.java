/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Planificador;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arny
 */
public class Fifo implements Runnable {

    ArrayList<Proceso> cola = new ArrayList<Proceso>();
    private Thread thread;
    private Planificador controlador;
    private int hz;

    public Fifo(ArrayList cola, Planificador controlador) {
        this.cola = (ArrayList) cola.clone();
        this.controlador = controlador;
        this.thread = new Thread(this, "Fifo");
    }

    public void start() {
        if (!this.thread.isAlive()) {
            this.thread.start();
        }

    }

    @Override
    public void run() {
        this.controlador.working();
        this.hz = 0;
        Proceso activo = null;
        Proceso varaux = null;
        Proceso var = null;
        boolean buc = true;

        do {
            Iterator<Proceso> itProceso = this.cola.iterator();
            while (itProceso.hasNext()) {
                varaux = itProceso.next();
                if (varaux.getcpuTres() == 0) {
                    if (!(varaux.getState() == 2)) {
                        varaux.setState(2);
                        varaux.seteT(this.hz);
                    }
                }
                if (!(varaux.getState() == 2)) {
                    if (varaux.getinT() <= this.hz) {
                        Iterator<Proceso> itProceso2 = this.cola.iterator();
                        while (itProceso2.hasNext()) {
                            var = itProceso2.next();
                            if (!varaux.equals(var)) {
                                if (!(var.getState() == 2)) {
                                    if (var.getState() == 0) {
                                        activo = varaux;
                                    }
                                    if (var.getState() == 1) {
                                        activo = var;
                                        break;
                                    }
                                } else {
                                    activo = varaux;
                                }
                            }
                        }
                        if (activo == varaux) {
                            if (activo.getsT() == -1) {
                                activo.setsT(this.hz);
                            }
                            activo.ejecutando();
                            if (activo.getState() == 0) {
                                activo.setState(1);
                            }
                        } else {
                            varaux.esperando();
                        }
//                        if (activo == null) {
//                            activo = varaux;
//                            //this
//                        } else {
//
//                        }
                    }
                }
            }
            activo = var = varaux = null;
      
            Iterator<Proceso> f = this.cola.iterator();
            while (f.hasNext()) {
                var = f.next();
                if (var.getState() == 2) {
                    buc = false;
                } else {
                    buc = true;
                    this.hz++;
                    try {
                        sleep(500);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Fifo.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
            }
            activo = var = varaux = null;
        } while (buc);
        System.out.println("Final Fifo");
        this.controlador.free();
    }

    public int getHz() {
        return this.hz;
    }
}
