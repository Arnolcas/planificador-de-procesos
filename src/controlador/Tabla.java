/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Proceso;
import vista.Vista;

/**
 *
 * @author Arny
 */
public class Tabla implements Runnable {

    Thread thread;
    private Vista vista;
    private ArrayList<Proceso> cola = new ArrayList<Proceso>();
    private Planificador controlador;
    private boolean runing = true;

    public Tabla(Vista vista, ArrayList cola, Planificador controlador) {
        this.vista = vista;
        this.cola = cola;
        this.controlador = controlador;
        this.thread = new Thread(this, "Refresh" + new Date().toString());
    }

    public void start() {
        if (!this.thread.isAlive()) {
            this.thread.start();
        } else {

            runing = false;
            while (this.thread.isAlive()) {
                System.out.println("esta vivo");
            }
            this.thread.start();
        }
    }

    @Override
    public void run() {
        do {
            Iterator<Proceso> itProceso = this.cola.iterator();
            for (int a = 0; itProceso.hasNext(); a++) {
                if (!this.runing) {
                    break;
                }
                try {
                    Proceso proceso = itProceso.next();

                    this.vista.jTable.getModel().setValueAt(proceso.getsT(), a, 4);
                    this.vista.jTable.getModel().setValueAt(proceso.geteT(), a, 5);
                    this.vista.jTable.getModel().setValueAt(proceso.getwT(), a, 6);
                    try {
                        this.vista.jLabel_hz.setText(this.controlador.getHzFifo() + "");
                    } catch (java.lang.NullPointerException ex) {
                        this.vista.jLabel_hz.setText(this.controlador.getHzSjf() + "");
                    }
                } catch (java.util.ConcurrentModificationException ex) {
                    this.runing = false;
                }
            }
            try {
                sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(Tabla.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (this.runing);
    }
}
