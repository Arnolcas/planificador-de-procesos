/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import modelo.*;
import vista.Vista;

/**
 *
 * @author Arny
 */
public class Planificador {

    private Vista vista;
    private ArrayList<Proceso> cola = new ArrayList<Proceso>();
    private Fifo fifo;
    private Sjf sjf;
    private Tabla refresh;
    private Promedio promedio;

    public Planificador() {
        this.vista = new Vista(this);
        this.vista.setVisible(true);
    }

    public void ejecutarFifo() {
        this.cola.clear();
        for (int a = 0; a < this.vista.cont; a++) {
            cola.add(new Proceso(this.vista.jTable.getModel().getValueAt(a, 0).toString(),
                    Integer.parseInt(this.vista.jTable.getModel().getValueAt(a, 1).toString()),
                    Integer.parseInt(this.vista.jTable.getModel().getValueAt(a, 2).toString()),
                    Integer.parseInt(this.vista.jTable.getModel().getValueAt(a, 3).toString())
            )
            );
//            JOptionPane.showMessageDialog(null, this.vista.jTable.getModel().getValueAt(a, 0).toString());
        }
        this.fifo = new Fifo(this.cola, this);
        this.fifo.start();
        this.refresh = new Tabla(vista, cola, this);
        this.refresh.start();
        this.promedio = new Promedio(this.vista, this.cola, this);
        this.promedio.start();
        this.sjf = null;

    }

    public void ejecutarSjf() {
        this.cola.clear();
        for (int a = 0; a < this.vista.cont; a++) {
            cola.add(new Proceso(this.vista.jTable.getModel().getValueAt(a, 0).toString(),
                    Integer.parseInt(this.vista.jTable.getModel().getValueAt(a, 1).toString()),
                    Integer.parseInt(this.vista.jTable.getModel().getValueAt(a, 2).toString()),
                    Integer.parseInt(this.vista.jTable.getModel().getValueAt(a, 3).toString())
            )
            );
//            JOptionPane.showMessageDialog(null, this.vista.jTable.getModel().getValueAt(a, 0).toString());
        }
        this.sjf = new Sjf(this.cola, this);
        this.sjf.start();
        this.refresh = new Tabla(vista, cola, this);
        this.refresh.start();
        this.promedio = new Promedio(this.vista, this.cola, this);
        this.promedio.start();
        this.fifo = null;
    }

    public int getHzFifo() {
        return this.fifo.getHz();
    }

    public int getHzSjf() {
        return this.sjf.getHz();
    }

    public void working() {
        this.vista.working();
    }

    public void free() {
        this.vista.free();
    }
}
