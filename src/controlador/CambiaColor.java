/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.awt.Color;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import vista.Vista;

/**
 *
 * @author Arny
 */
public class CambiaColor implements Runnable{
    private JTextField textfield;
    private Thread thread;
    
    public CambiaColor(JTextField textfield){
        this.textfield = textfield;
        this.thread = new Thread(this, "Cambia Color");                
    }
    
    public void start(){
        if(!this.thread.isAlive()){
            this.thread.start();
        }
    }
    @Override
    public void run() {
        this.textfield.setBackground(Color.red);
        try {
            sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(CambiaColor.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.textfield.setBackground(Color.WHITE);
    }
    
}
