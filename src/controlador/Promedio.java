/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Proceso;
import vista.Vista;

/**
 *
 * @author Arny
 */
public class Promedio implements Runnable {

    Thread thread;
    private Vista vista;
    private ArrayList<Proceso> cola;
    private Planificador controlador;
    private boolean runing = true;

    public Promedio(Vista vista, ArrayList cola, Planificador controlador) {
        this.vista = vista;
        this.cola = cola;
        this.controlador = controlador;
        this.thread = new Thread(this, "Promedio" + new Date().toString());
    }

    public void start() {
        if (!this.thread.isAlive()) {
            this.thread.start();
        } else {

            runing = false;
            while (this.thread.isAlive()) {
                System.out.println("esta vivo");
            }
            this.thread.start();
        }
    }

    @Override
    public void run() {
        do {
            int prom = 0;
            Iterator<Proceso> itProceso = this.cola.iterator();
            while(itProceso.hasNext()){
                prom = prom + itProceso.next().getwT();
            }
            for(int a = 0; a<this.cola.size(); a++){
                this.vista.jTable.getModel().setValueAt(((float) prom/this.cola.size())+"", a, 7);
            }
            try {
                sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(Promedio.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (this.runing);
    }
}
